CREATE SCHEMA si6;
    ALTER SCHEMA si6 OWNER TO "gregory.david";

SET search_path TO si6;

CREATE TYPE genre AS ENUM (
	'Femelle',
	'Mâle',
	'Intersexué'
	);

CREATE TABLE "Animal" (
	"id" serial,
	"sexe" genre DEFAULT NULL,
	"date_naissance" date DEFAULT NULL,
	"nom" character varying(20) COLLATE "fr_FR.utf8" DEFAULT NULL,
	"commentaires" text COLLATE "fr_FR.utf8",
	"espece_id" smallint NOT NULL,
	"race_id" smallint DEFAULT NULL,
	"mere_id" smallint DEFAULT NULL,
	"pere_id" smallint DEFAULT NULL
	);

CREATE TABLE "Espece" (
	"id" serial,
	"nom_courant" character varying(40) COLLATE "fr_FR.utf8" NOT NULL,
	"nom_latin" character varying(40) COLLATE "fr_FR.utf8" NOT NULL,
	"description" text COLLATE "fr_FR.utf8",
	"prix" numeric(7,2) DEFAULT NULL
	);

CREATE TABLE "Race" (
	"id" serial,
	"nom" character varying(40) COLLATE "fr_FR.utf8" NOT NULL,
	"espece_id" smallint NOT NULL,
	"description" text COLLATE "fr_FR.utf8",
	"prix" numeric(7,2) DEFAULT NULL
	);

CREATE UNIQUE INDEX "unique_nom_espece_id" ON "Animal" ("nom","espece_id");
CREATE INDEX "fk_animal_espece_id" ON "Animal" ("espece_id");
CREATE INDEX "fk_animal_race_id" ON "Animal" ("race_id");
CREATE INDEX "fk_animal_mere_id" ON "Animal" ("mere_id");
CREATE INDEX "fk_animal_pere_id" ON "Animal" ("pere_id");

CREATE UNIQUE INDEX "unique_nom_latin" ON "Espece" ("nom_latin");

CREATE INDEX "fk_race_espece_id" ON "Race" ("espece_id");

    ALTER TABLE "Animal" ADD PRIMARY KEY ("id");
    ALTER TABLE "Race" ADD PRIMARY KEY ("id");
    ALTER TABLE "Espece" ADD PRIMARY KEY ("id");

    ALTER TABLE "Animal" ADD CONSTRAINT "fk_animal_pere_id" FOREIGN KEY ("pere_id") REFERENCES "Animal" ("id");
    ALTER TABLE "Animal" ADD CONSTRAINT "fk_animal_espece_id" FOREIGN KEY ("espece_id") REFERENCES "Espece" ("id");
    ALTER TABLE "Animal" ADD CONSTRAINT "fk_animal_mere_id" FOREIGN KEY ("mere_id") REFERENCES "Animal" ("id");
    ALTER TABLE "Animal" ADD CONSTRAINT "fk_animal_race_id" FOREIGN KEY ("race_id") REFERENCES "Race" ("id");

    ALTER TABLE "Race" ADD CONSTRAINT "fk_race_espece_id" FOREIGN KEY ("espece_id") REFERENCES "Espece" ("id");
    ALTER TABLE "Race" ADD CONSTRAINT "prix_positif" CHECK (prix > 0);

    ALTER TABLE "Espece" ADD CONSTRAINT "prix_positif" CHECK (prix > 0);
